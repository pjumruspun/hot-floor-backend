package main

import (
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis"
	uuid "github.com/satori/go.uuid"
)

func IsEmpty(data string) bool {
	return len(data) == 0
}

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	mux := http.NewServeMux()

	mux.HandleFunc("/register", func(response http.ResponseWriter, request *http.Request) {
		request.ParseForm()
		for key, value := range request.Form {
			fmt.Printf("%s = %s\n", key, value)
		}

		name := request.FormValue("name")

		if IsEmpty(name) {
			fmt.Println("Name empty")
			fmt.Fprintf(response, "Name cannot be empty!")
			return
		}

		id := uuid.NewV4()
		stringId := hex.EncodeToString(id.Bytes())
		fmt.Printf("Generated UUID: %x\n", id)
		fmt.Printf("Hex encode UUID: %s\n", stringId)
		fmt.Printf("Name: %s\n", name)

		err := client.Set(stringId, name, 0).Err()
		if err != nil {
			fmt.Println(err)
		}

		fmt.Fprintf(response, stringId)
	})

	mux.HandleFunc("/leaderboards/add", func(response http.ResponseWriter, request *http.Request) {
		request.ParseForm()

		id := request.FormValue("id")
		score := request.FormValue("score")

		if IsEmpty(id) || IsEmpty(score) {
			fmt.Println("There is an empty data")
			fmt.Fprintf(response, "There is an empty data")
			return
		}

		err := client.Get(id).Err()
		if err == redis.Nil {
			fmt.Println("UUID does not exist")
			fmt.Fprintf(response, "UUID does not exist")
			return
		}

		scoreF64, err := strconv.ParseFloat(score, 64)
		if err != nil {
			fmt.Println(err)
		}

		if scoreF64 <= 0 {
			fmt.Println("Score must be greater than 0 to be saved!")
			fmt.Fprintf(response, "Score must be greater than 0 to be saved!")
			return
		}

		foundData := client.ZScore("leaderboards", id)
		if foundData == nil {
			// First time adding to leaderboards
			fmt.Println("UUID not found, adding to leaderboards")
		} else {
			if foundData.Val() >= scoreF64 {
				// Score in leaderboard is already better or equal
				fmt.Printf(
					"Score in leaderboards: %f\n Score trying to save: %f\n (will not be saved)",
					foundData.Val(),
					scoreF64)
				fmt.Fprintf(
					response,
					"Score in leaderboards (%f) is higher or equal than score trying to save (%f)",
					foundData.Val(),
					scoreF64)
				return
			}
		}

		// Score should be saved from here
		err = client.ZAdd("leaderboards", redis.Z{scoreF64, id}).Err()
		if err != nil {
			fmt.Println(err)
			fmt.Fprintf(response, "Failing to add %s with score = %f", id, scoreF64)
		}

		fmt.Fprintf(response, "Succeeding in adding %s with score = %f", id, scoreF64)
	})

	mux.HandleFunc("/leaderboards", func(response http.ResponseWriter, request *http.Request) {
		request.ParseForm()

		// Check value in request's form
		for key, value := range request.Form {
			fmt.Printf("%s = %s\n", key, value)
		}

		count := request.FormValue("count")
		countInt, err := strconv.ParseInt(count, 10, 64)

		if err != nil {
			countInt = 5
		}

		result, err := client.ZRevRangeWithScores("leaderboards", 0, countInt-1).Result()
		if err != nil {
			fmt.Printf("Error retrieving top %d keys: %v", countInt, err)
		}

		stringResult := ""
		for _, zItem := range result {
			resultName, err := client.Get(fmt.Sprintf("%v", zItem.Member)).Result()
			resultScore := fmt.Sprintf("%v", zItem.Score)
			if err != nil {
				fmt.Println(err)
			}

			fmt.Printf("%v %v\n", resultName, resultScore)
			stringResult += resultName + " " + resultScore + "\n"
		}

		fmt.Println(stringResult)

		fmt.Fprintf(response, stringResult)
	})

	http.ListenAndServe(":8080", mux)
}
