package main

import (
	"fmt"
	"testing"

	"github.com/alicebob/miniredis/v2"

	"github.com/go-redis/redis"
)

func CheckLeaderboard(t *testing.T, client *redis.Client) {
	tags := map[string]float64{
		"python":   3,
		"memcache": 1,
		"rust":     2,
		"c":        1,
		"redis":    1,
		"software": 1,
		"docker":   1,
		"go":       1,
		"linux":    1,
		"flask":    1,
	}

	for tag, score := range tags {
		_, err := client.ZAdd("tags", redis.Z{score, tag}).Result()
		if err != nil {
			t.Errorf("Error adding %s", tag)
		}
	}

	result, err := client.ZRevRangeWithScores("tags", 0, 4).Result()
	if err != nil {
		t.Errorf("Error retrieving top 5 keys: %v", err)
	}
	for _, zItem := range result {
		t.Logf("%v %v\n", zItem.Member, zItem.Score)
		actualValue, found := tags[fmt.Sprintf("%v", zItem.Member)]
		if !found {
			t.Errorf("Tags \"%v\" does not exist", zItem.Member)
		} else if actualValue != zItem.Score {
			t.Errorf("Expected %v, got %v", fmt.Sprintf("%v", zItem.Member), actualValue)
		}
	}
}

func TestLeaderboard(t *testing.T) {
	r, err := miniredis.Run()
	if err != nil {
		t.Fatal("Redis run failed")
	}

	client := redis.NewClient(&redis.Options{
		Addr: r.Addr(),
	})

	//Use client as you used normally
	CheckLeaderboard(t, client)
}
