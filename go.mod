module bitbucket.org/pjumruspun/register-login-test

go 1.14

require (
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/satori/go.uuid v1.2.0
	github.com/alicebob/miniredis/v2 v2.12.0
)
